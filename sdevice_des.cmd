

File {
	Grid = "@tdr@"
	Plot = "n@node@"
	Current = "width_@width@_dopingw_@doping_width@_thick_@thickness@_bias_@Bias@.plt"
	Parameter = "@parameter@"

}


Electrode{
{ Name="hv"  Voltage=0.0 }
{ Name="pix" Voltage=0.0 }


}


Physics{
	*DriftDiffusion
	eQCvanDort
	AreaFactor=1
	EffectiveIntrinsicDensity( OldSlotBoom )
	Mobility(
		DopingDep
		eHighFieldsaturation( GradQuasiFermi )
		hHighFieldsaturation( GradQuasiFermi )
		Enormal
	)
	Recombination(
		SRH( DopingDep )
	)
		
}




      
     
Plot{
	eDensity hDensity
	ElectricField/Vector 
	Potential 
	SpaceCharge
	eTrappedCharge hTrappedCharge
	Doping DonorConcentration AcceptorConcentration 
	HeavyIonChargeDensity
	CurrentPotential
	hInterfaceTrappedCharge
}



Math {
	Number_Of_Threads=4
	Number_of_Solver_Threads = 4
	Extrapolate
	Derivatives
	Iterations=25
	RelErrControl
	Digits=10
	Method=pardiso
	NotDamped=200
	CheckTransientError
	CurrentWeighting
	RecBoxIntegr
	RhsFactor=1e14

}


Solve {
	
	QuasiStationary(Goal{ Name="hv" Voltage=@Bias@}) {
	Coupled{ Poisson electron hole} 
	CurrentPlot }			
	Save(FilePrefix="dcsol_n@node|sdevice@")
}



