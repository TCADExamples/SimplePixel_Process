

File {
	Grid = "@tdr@"
	Load ="dcsol_n@node|sdevice@_des"
	Plot = "n@node@"
	Current = "width_@width@_dopingw_@doping_width@_thick_@thickness@_bias_@Bias@.plt"
	Parameter = "@parameter@"

}


Electrode{
{ Name="hv"  Voltage=@Bias@ }
{ Name="pix" Voltage=0.0 }


}


Physics{
	*DriftDiffusion
	eQCvanDort
	AreaFactor=1
	EffectiveIntrinsicDensity( OldSlotBoom )
	Mobility(
		DopingDep
		eHighFieldsaturation( GradQuasiFermi )
		hHighFieldsaturation( GradQuasiFermi )
		Enormal
	)
	Recombination(
		SRH( DopingDep )
	)
	
	HeavyIon ( Direction=(1,0.1) Location=(0,15) 
	Time=5.0e-10 Length=300 Wt_hi=0.5 LET_f=0.00001 
	Gaussian PicoCoulomb )
	
}

Physics (MaterialInterface="Silicon/Oxide") {Charge(Conc=1e10)}


Plot{
	eDensity hDensity
	ElectricField/Vector 
	Potential 
	SpaceCharge
	eTrappedCharge hTrappedCharge
	Doping DonorConcentration AcceptorConcentration HeavyIonChargeDensity
	CurrentPotential
}



Math {
	Number_Of_Threads=8
	Number_of_Solver_Threads = 8
	Extrapolate
	Derivatives
	Iterations=10
	RelErrControl
	Digits=6
	Method=pardiso
	NotDamped=200
	CheckTransientError
	CurrentWeighting
	RecBoxIntegr
	RhsFactor=1e10

}



Solve {
				
	Transient( InitialTime = 0.0 FinalTime = 10e-9 Plot 
	{ Range = (0 10e-9) Intervals=20 } ) 
	{ Coupled { Poisson Electron Hole } }

}
