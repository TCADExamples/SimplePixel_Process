math coord.ucs


AdvancedCalibration 2013.12

#Medipix footprint data

#set NDose 1e15

#set hwidth @<@width@/2>@
#set hdoping_width @<@doping_width@/2>@
#set hmetal_width @<@metal_width@/2>@
#set y_mesh_left  @<@hwidth@ - @hdoping_width@ - 2.5>@
#set y_mesh_right @<@hwidth@ - @hdoping_width@ + 2.5>@

#set half_thickness @<@thickness@/2.0>@
#set p1 @< @thickness@ -0.05 >@
#set p2 @< @thickness@ -0.5 >@
#set p3 @< @thickness@ -2.0 >@
#set p4 @< @thickness@ -4.0 >@
#set p5 @< @thickness@ -10.0 >@



line x location= 0.0     spacing= 2.0<nm> tag=SiTop        
line x location=50.0<nm> spacing=50.0<nm>
line x location= 0.5<um> spacing=100.0<nm>                      
line x location= 2.0<um> spacing= 0.2<um>                       
line x location= 4.0<um> spacing= 0.5<um>
line x location=10.0<um> spacing= 2.0<um> 
line x location=@half_thickness@ spacing= 5.0<um> 
line x location=@p5@ spacing= 2.0<um>
line x location=@p4@ spacing= 0.5<um>
line x location=@p3@ spacing= 0.2<um>                       
line x location=@p2@ spacing= 100.0<nm>                      
line x location=@p1@ spacing=50.0<nm>                      
line x location=@thickness@ spacing= 2.0<nm> tag=SiBottom 

line y location=0.0      spacing=5.0<um>  tag=leftEdge        
line y location=@y_mesh_left@ spacing=300.0<nm> tag=leftDopingEdgeIn 
line y location=@y_mesh_right@ spacing=300.0<nm> tag=leftDopingEdgeout
line y location=@hwidth@ spacing=5.0<um>  tag=middle

math numThreads=2
pdbSet Silicon Phosphorus ActiveModel None
pdbSet Silicon Boron ActiveModel None


region Silicon xlo= SiTop xhi= SiBottom
init resistivity=10000 field=Phosphorus 

deposit thickness=0.2 oxide anisotropic

#set y_pstop_left  @<(@hwidth@ - @hdoping_width@ - 5)>@

mask name=pstop left=@y_pstop_left@ right=@width@ negative
photo mask=pstop thickness=1

implant Boron dose=1e12 energy=60 tilt=0 rotation=0
strip resist
struct tdr=after_pstop

#set y_doping_left  @<(@hwidth@ - @hdoping_width@)>@



mask name=doping left=@y_doping_left@ right=@width@ 

photo mask=doping thickness=1

implant Phosphorus dose=@NDose@ energy=120 tilt=0 rotation=0

strip resist

struct tdr=after_nimplant

transform flip


deposit thickness=0.04 oxide anisotropic
implant Boron dose=1e15 energy=60 tilt=0 rotation=0

diffuse temperature=940<C> time=240<min>

etch material = {oxide} type=anisotropic rate=0.04 time=1
deposit thickness=0.8<um> aluminum isotropic region.name=HV

transform flip

struct tdr=after_backside


#set hcontact_width @<@contact_width@/2>@
#set x_contact  @<(@hwidth@ - @hcontact_width@)>@
mask name=contact left=0 right=@x_contact@
etch material = {oxide} type=anisotropic rate=0.2 time=1 mask=contact


#set x_metaledge  @<(@hwidth@ -@hmetal_width@)>@
mask left=0 right=@x_metaledge@ name=alu
deposit thickness=0.8<um> aluminum isotropic mask=alu region.name=PIX 

contact name=hv region=HV
contact name=pix region=PIX

refinebox clear
refinebox !keep.lines
# reset default settings for adaptive meshing
pdbSet Grid AdaptiveField Refine.Abs.Error     1e37
pdbSet Grid AdaptiveField Refine.Rel.Error     1e10
pdbSet Grid AdaptiveField Refine.Target.Length 100.0
pdbSet Grid Adaptive 1
pdbSet Grid SnMesh DelaunayType boxmethod

refinebox name= Global refine.min.edge= {0.01 0.01} refine.max.edge= {0.4 0.4} refine.fields= { NetActive Boron Phosphorus} def.max.asinhdiff= 0.3 adaptive all add
grid remesh


transform reflect right

struct tdr=n@node@ 

exit



